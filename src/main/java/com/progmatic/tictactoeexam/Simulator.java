package com.progmatic.tictactoeexam;

import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.exceptions.CellException;
import com.progmatic.tictactoeexam.interfaces.Player;
import java.io.FileNotFoundException;

/**
 *
 * @author User
 */
public class Simulator {

    public static void main(String[] args) throws FileNotFoundException {
        
        BigBoard board3x3 = new BigBoard(8, 8, 4);
        Player player1, player2;
        //MinimaxPlayer mmp1, mmp2;
        player1 = new MinimaxPlayer(PlayerType.O);
        player2 = new MinimaxPlayer(PlayerType.X);
        
        //player1 = new SimplePlayer(PlayerType.O);
        //Player player2 = new VictoryAwarePlayer(PlayerType.X);
        try{
            //board3x3.put(new Cell(0, 1, PlayerType.X));
            //board3x3.put(new Cell(1, 2, PlayerType.X));
            //board3x3.put(new Cell(2, 3, PlayerType.X));
        for (int i = 0; i < board3x3.ROWS * board3x3.COLUMNS; i++) {
            Cell nextCell;
            PlayerType p;
            if( i % 2 == 1){
                p = player1.getMyType();
                nextCell = player1.nextMove(board3x3);
            }else{
                p = player2.getMyType();
                nextCell = player2.nextMove(board3x3);
            }
            
            board3x3.put(nextCell);
            System.out.println( (i+1) + ". round: " + p.toString() + "'s step: " +  nextCell );
            board3x3.printBoard();
            ImaginaryBoard ib = new ImaginaryBoard(board3x3);
            System.out.print( "value O: " + ib.getSomeValue(PlayerType.O) );
            System.out.println( "\tvalue X: " + ib.getSomeValue(PlayerType.X) );
            if(board3x3.hasWon(p)){
                System.out.println( p.toString() + " won.");
                break;
            }
        }
        }catch(CellException ex){
            System.out.println( ex );
        }catch(NullPointerException ex){
            System.out.println( ex );
        }
        System.out.println("-- END --");
        if(player1 instanceof MinimaxPlayer){
         ((MinimaxPlayer) player1).close();
        }
        if(player2 instanceof MinimaxPlayer){
         ((MinimaxPlayer) player2).close();
        }
    }
    
}
