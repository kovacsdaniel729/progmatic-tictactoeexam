package com.progmatic.tictactoeexam;

import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.exceptions.CellException;
import com.progmatic.tictactoeexam.interfaces.Board;
import java.util.List;

/**
 *
 * @author Kovacs Daniel
 */
public class ImaginaryBoard extends BigBoard{

    //copies board, allows modification
    public ImaginaryBoard(Board b){
        super(BigBoard.getNumberOfRows(b), BigBoard.getNumberOfColumns(b), getNumberOfMarks(b));
        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLUMNS; j++) {
                try{
                    put( new Cell(i, j, b.getCell(i, j)) );
                }
                catch(CellException ex){
                }
            }
        }
    }

    @Override
    public void put(Cell cell) {
        cells[cell.getRow()][cell.getCol()] = cell.getCellsPlayer();
    }
    
    public int getSomeValue(PlayerType p){
        int value = 0;
        int count, maxCount=0;
        for (int i = 0; i < ROWS; i++) {
            count=0;
            for (int j = 0; j < COLUMNS; j++) {
                if( cells[i][j].equals(p) ){
                    count++;
                    if( count > maxCount){
                        maxCount = count;
                    }
                }else{
                    value += count * count;
                    count=0;
                }
            }
        }
        for (int j = 0; j < COLUMNS; j++) {
            count=0;
            for (int i = 0; i < ROWS; i++) {
                if( cells[i][j].equals(p) ){
                    count++;
                    if( count > maxCount){
                        maxCount = count;
                    }
                }else{
                    value += count * count;
                    count=0;
                }
            }
        }
        //diagonals = rows + cols - 1
        for (int i = NUMBER_OF_MARKS-1; i < ROWS + COLUMNS - NUMBER_OF_MARKS ; i++) {
            count=0;
            int startRow = Math.max(ROWS-1-i, 0), startCol = Math.max(i-ROWS+1, 0);
            for (int j = 0; j < Math.min(ROWS-startRow, COLUMNS-startCol); j++) {
                if( cells[startRow+j][startCol+j].equals(p) ){
                    count++;
                    if( count > maxCount){
                        maxCount = count;
                    }
                }else{
                    value += count * count;
                    count=0;
                }
            }
        }
        //vertically mirror, col -> COLUMNS - col - 1
        for (int i = NUMBER_OF_MARKS-1; i < ROWS + COLUMNS - NUMBER_OF_MARKS ; i++) {
            count=0;
            int startRow = Math.max(ROWS-1-i, 0), startCol = Math.max(i-ROWS+1, 0);
            for (int j = 0; j < Math.min(ROWS-startRow, COLUMNS-startCol); j++) {
                if( cells[startRow+j][COLUMNS - startCol-j - 1].equals(p) ){
                    count++;
                    if( count > maxCount){
                        maxCount = count;
                    }
                }else{
                    value += count * count;
                    count=0;
                }
            }
        }
        return value;
        //return maxCount;
    }
    
    public int countMyType(PlayerType p){
        int count = 0;
        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLUMNS; j++) {
                if(cells[i][j].equals(p)){
                    count++;
                }
            }
        }
        return count;
    }
    
}
