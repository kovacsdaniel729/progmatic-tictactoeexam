package com.progmatic.tictactoeexam;

import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.exceptions.CellException;
import com.progmatic.tictactoeexam.interfaces.AbstractPlayer;
import com.progmatic.tictactoeexam.interfaces.Board;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Kovacs Daniel
 */
public class MinimaxPlayer extends AbstractPlayer{

    private PrintStream pw;
    private String fileName;
    private int step=0;
    
    protected List<GameTree> steps = new ArrayList<>();
    
    public MinimaxPlayer(PlayerType p) throws FileNotFoundException {
        super(p);
        fileName = "src/player" + p.toString() + ".txt";
        pw = new PrintStream(new File(fileName));
    }

    
    @Override
    public Cell nextMove(Board b) {
        //Cell bestCell;
        //int bestValue = Integer.MIN_VALUE;
        List<Cell> emptyCells = b.emptyCells();
        if(emptyCells.isEmpty()){
            return null;
        }
        ImaginaryBoard myBoard = new ImaginaryBoard(b);
        if(myBoard.countMyType(myType) == 0 && myBoard.countMyType(myType.invert()) == 0 ){
            return new Cell(myBoard.ROWS/2, myBoard.COLUMNS/2, myType);
        }
        steps.clear();
        GameTree root = new GameTree(-1, -1);
        makeGameTree(myBoard, root, 3);//depth of tree
        //root.setToChildMax();
        step++;
        pw.println("-----------------------------------");
        pw.println( step+ " ---------------------------------");
        pw.println("-----------------------------------");
        //printGameTree(root);
        printChild(root);
        for (GameTree child : root.children) {
            if(child.value == root.value){
                return new Cell(child.row, child.column, myType);
            }
        }
        return null;
    }
    
    protected void makeGameTree(ImaginaryBoard b, GameTree parent, int level){
        for (Cell cell : b.emptyCells()) {
            if( isAway(b, cell) ){continue;}
            GameTree current = new GameTree(cell.getRow(), cell.getCol());
            parent.addChild(current);
            ImaginaryBoard myBoard = new ImaginaryBoard(b);
            if(level % 2 == 1){//
                cell.setCellsPlayer(myType);
            }else{
                cell.setCellsPlayer(myType.invert());
            }
            myBoard.put(cell);
            boolean isWinner = myBoard.hasWon(myType) || myBoard.hasWon(myType.invert());
            if(level > 0 && !isWinner){
                makeGameTree(myBoard, current, level-1);
            }else {
                current.value = calcBoardValue(myBoard);
            }
            cell.setCellsPlayer(PlayerType.EMPTY);
            myBoard.put(cell);
        }
        if (!parent.children.isEmpty()) {
            if (level % 2 == 0) {
                parent.setToChildMin();
            } else {
                parent.setToChildMax();
            }
        } else {
            parent.value = calcBoardValue(b);
        }
        
    }
    
    protected int calcBoardValue(ImaginaryBoard b){
        int myValue =  b.getSomeValue(myType), enemyValue = b.getSomeValue(myType.invert());
        if( b.hasWon(myType.invert()) ){
            return -400*b.NUMBER_OF_MARKS * b.NUMBER_OF_MARKS + b.countMyType(myType);
        }
        if( b.hasWon(myType) ){
            return 400*b.NUMBER_OF_MARKS * b.NUMBER_OF_MARKS - b.countMyType(myType.invert());
        }
        return myValue - enemyValue;
    }
    
    
    
    protected void printGameTree(GameTree root){
        printGameTree(root, 0);
    }
    
    protected void printChild(GameTree node){
        pw.println("row=" + node.row + ", col=" + node.column + ", value=" + node.value);
        for (GameTree child : node.children) {
             pw.println("\trow=" + child.row + ", col=" + child.column + ", value=" + child.value);
        }
        pw.println();
    }
    
    private void printGameTree(GameTree node, int level){
        pw.println("row=" + node.row + ", col=" + node.column + ", value=" + node.value);
        for (GameTree child : node.children) {
            for (int i = 0; i < level; i++) {
                pw.print("\t");
            }
            printGameTree(child, level+1);
        }
    }
    
    protected class GameTree{
        
        public final int row,column;
        public int value;
        public List<GameTree> children = new ArrayList<>();
        
        public GameTree(int row, int column ){
            this.row = row;
            this.column = column;
        }
        
        public void addChild(GameTree child){
            children.add(child);
        }
        
        public void setToChildMin(){
            int min = Integer.MAX_VALUE;
            for (GameTree child : children) {
                if( child.value < min ){
                    min = child.value;
                }
            }
            value = min;
        }
        
        public void setToChildMax(){
            int max = Integer.MIN_VALUE;
            for (GameTree child : children) {
                if( child.value > max ){
                    max = child.value;
                }
            }
            value = max;
        }
    }
    
    public void close(){
        pw.close();
    }
    
    protected boolean isAway(ImaginaryBoard b, Cell c){
        int count=0;
        int row = c.getRow(), col = c.getCol();
        for (int i = -1; i < 2; i++) {
            for (int j = -1; j < 2; j++) {
                try {
                    if(  !b.getCell(row+i, col+j).equals(PlayerType.EMPTY) ){
                        count++;
                    }
                } catch (CellException ex) {
                }
            }
        }
        return (count == 0);
    }
    
}
