package com.progmatic.tictactoeexam;

import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.exceptions.CellException;
import com.progmatic.tictactoeexam.interfaces.Board;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Kovacs Daniel
 */
public class BigBoard implements Board{

    public final int ROWS ;
    public final int COLUMNS ;
    public final int NUMBER_OF_MARKS ;    //to win
    
    protected PlayerType[][] cells;     

    public BigBoard() {
        this(3,3,3);
    }

    public BigBoard(int ROWS, int COLUMNS, int NUMBER_OF_MARKS) {
        this.ROWS = ROWS;
        this.COLUMNS = COLUMNS;
        this.NUMBER_OF_MARKS = NUMBER_OF_MARKS;
        cells = new PlayerType[ROWS][COLUMNS];     
        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLUMNS; j++) {
                cells[i][j] = PlayerType.EMPTY;
            }
        }
    }
    
    
    public boolean isValidCell(int rowIdx, int colIdx){
        return rowIdx >= 0 &&  rowIdx < ROWS && colIdx >=0 && colIdx < COLUMNS;
    }
    
    public boolean isValidCell(Cell cell){
        return isValidCell(cell.getRow(), cell.getCol());
    }
    
    protected void checkCell(int rowIdx, int colIdx) throws CellException{
        if( !( rowIdx >= 0 &&  rowIdx < ROWS && colIdx >=0 && colIdx < COLUMNS ) ){
            throw new CellException(rowIdx, colIdx, "invalid cell.");
        }
    }
    
    @Override
    public PlayerType getCell(int rowIdx, int colIdx) throws CellException {
        checkCell(rowIdx, colIdx);
        return cells[rowIdx][colIdx];
    }

    @Override
    public void put(Cell cell) throws CellException {
        int rowIdx = cell.getRow(), colIdx = cell.getCol();
        checkCell(rowIdx, colIdx);
        if( !cells[rowIdx][colIdx].equals(PlayerType.EMPTY) ){
            throw new CellException(rowIdx, colIdx, "this cell is already occupied.");
        }else{
            cells[rowIdx][colIdx] = cell.getCellsPlayer();
        }
    }

    @Override
    public boolean hasWon(PlayerType p) {
        //return (maxMarkLength(p) >= NUMBER_OF_MARKS);
        int count;
        for (int i = 0; i < ROWS; i++) {
            count=0;
            for (int j = 0; j < COLUMNS; j++) {
                if( cells[i][j].equals(p) ){
                    count++;
                    if( count == NUMBER_OF_MARKS){
                        return true;
                    }
                }else{
                    count=0;
                }
            }
        }
        for (int j = 0; j < COLUMNS; j++) {
            count=0;
            for (int i = 0; i < ROWS; i++) {
                if( cells[i][j].equals(p) ){
                    count++;
                    if( count == NUMBER_OF_MARKS){
                        return true;
                    }
                }else{
                    count=0;
                }
            }
        }
        //diagonals = rows + cols - 1
        for (int i = NUMBER_OF_MARKS-1; i < ROWS + COLUMNS - NUMBER_OF_MARKS ; i++) {
            count=0;
            int startRow = Math.max(ROWS-1-i, 0), startCol = Math.max(i-ROWS+1, 0);
            for (int j = 0; j < Math.min(ROWS-startRow, COLUMNS-startCol); j++) {
                if( cells[startRow+j][startCol+j].equals(p) ){
                    count++;
                    if( count == NUMBER_OF_MARKS){
                        return true;
                    }
                }else{
                    count=0;
                }
            }
        }
        //vertically mirror, col -> COLUMNS - col - 1
        for (int i = NUMBER_OF_MARKS-1; i < ROWS + COLUMNS - NUMBER_OF_MARKS ; i++) {
            count=0;
            int startRow = Math.max(ROWS-1-i, 0), startCol = Math.max(i-ROWS+1, 0);
            for (int j = 0; j < Math.min(ROWS-startRow, COLUMNS-startCol); j++) {
                if( cells[startRow+j][COLUMNS - startCol-j - 1].equals(p) ){
                    count++;
                    if( count == NUMBER_OF_MARKS){
                        return true;
                    }
                }else{
                    count=0;
                }
            }
        }
        return false;
    }

    @Override
    public List<Cell> emptyCells() {
        List<Cell> emptyCells = new ArrayList<>();
        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLUMNS; j++) {
                if( cells[i][j].equals(PlayerType.EMPTY) ){
                    emptyCells.add(new Cell(i, j));
                }
            }
        }
        return emptyCells;
    }
    
    public static int getNumberOfRows(Board b){
        int rows=0;
        while(true){
            try {
                rows++;
                b.getCell(rows, 0);
            } catch (CellException ex) {
                break;
            }
        }
        return rows;
    }
    
    public static int getNumberOfColumns(Board b){
        int cols=0;
        while(true){
            try {
                cols++;
                b.getCell(0, cols);
            } catch (CellException ex) {
                break;
            }
        }
        return cols;
    }
    
    public static int getNumberOfMarks(Board b){
        if( b instanceof BigBoard){
            return ((BigBoard) b).NUMBER_OF_MARKS;
        }else{
            return 3;
        }
    }
    
    public void printBoard(){
        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLUMNS; j++) {
                if(cells[i][j].equals(PlayerType.EMPTY)){
                    System.out.print("·");
                }else{
                    System.out.print( cells[i][j].toString() );
                }
            }
            System.out.println("");
        }
    }
    
}
