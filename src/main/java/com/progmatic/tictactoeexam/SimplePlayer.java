package com.progmatic.tictactoeexam;

import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.interfaces.AbstractPlayer;
import com.progmatic.tictactoeexam.interfaces.Board;
import java.util.List;

/**
 *
 * @author Kovacs Daniel
 */
public class SimplePlayer extends AbstractPlayer{

    public SimplePlayer(PlayerType p) {
        super(p);
    }

    @Override
    public Cell nextMove(Board b) {
        List<Cell> emptyCells = b.emptyCells();
        if( emptyCells.isEmpty() ){
            return  null;
        }
        Cell firstEmpty = emptyCells.get(0);
        return new Cell( firstEmpty.getRow() ,  firstEmpty.getCol() , myType);
    }
    
}
