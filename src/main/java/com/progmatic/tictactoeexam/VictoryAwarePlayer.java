package com.progmatic.tictactoeexam;

import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.exceptions.CellException;
import com.progmatic.tictactoeexam.interfaces.AbstractPlayer;
import com.progmatic.tictactoeexam.interfaces.Board;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Kovacs Daniel
 */
public class VictoryAwarePlayer extends AbstractPlayer{

    public VictoryAwarePlayer(PlayerType p) {
        super(p);
    }
    
    @Override
    public Cell nextMove(Board b) {
        ImaginaryBoard myBoard = new ImaginaryBoard(b);
        List<Cell> emptyCells = myBoard.emptyCells();
        if( emptyCells.isEmpty() ){
            return null;
        }
        for (Cell cell : emptyCells) {
                cell.setCellsPlayer(myType);
                myBoard.put(cell);
                boolean isWin = myBoard.hasWon(myType);
                cell.setCellsPlayer(PlayerType.EMPTY);
                myBoard.put(cell);
                if (isWin) {
                    return new Cell(cell.getRow(), cell.getCol(), myType);
                }
        }
        Cell firstEmpty = emptyCells.get(0);
        return new Cell( firstEmpty.getRow() ,  firstEmpty.getCol() , myType);
    }
    
    
}
